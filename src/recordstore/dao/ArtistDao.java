package recordstore.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import recordstore.database.ChinookDatabase;
import recordstore.models.Artist;

public class ArtistDao {

    public List<Artist> getAllArtists() {
        // TODO: Siirra oliomuuttujaksi, olio annetaan konstruktoriparametrina
        ChinookDatabase db = new ChinookDatabase();

        Connection connection = db.connect();
        PreparedStatement statement = null;
        ResultSet results = null;

        List<Artist> list = new ArrayList<>();

        try {
            statement = connection.prepareStatement("SELECT * FROM Artist ORDER BY Name ASC");
            results = statement.executeQuery();

            while (results.next()) {
                long id = results.getLong("ArtistId");
                String name = results.getString("Name");
                list.add(new Artist(id, name));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        /*
         * TODO: siirra resurssien sulkeminen finally-lohkoon, jotta resurssit suljetaan
         * vaikka metodista heitettaisiin poikkeus.
         */
        db.close(results, statement, connection);

        return list;
    }
}
