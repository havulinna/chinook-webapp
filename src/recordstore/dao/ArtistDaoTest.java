package recordstore.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;

import org.junit.jupiter.api.Test;

import recordstore.models.Artist;

public class ArtistDaoTest {

    /* TODO: Siirra ArtistDao oliomuuttujaksi, joka alustetaan @Before-metodissa */

    @Test
    public void testArtistListIsNotEmpty() {
        ArtistDao dao = new ArtistDao();
        List<Artist> all = dao.getAllArtists();

        assertFalse(all.isEmpty()); // sama kuin assertEquals(false, all.isEmpty());
    }

    @Test
    public void testTheNameOfTheFirstArtist() {
        ArtistDao dao = new ArtistDao();
        List<Artist> all = dao.getAllArtists();

        assertEquals("A Cor Do Som", all.get(0).getName());

    }
}
